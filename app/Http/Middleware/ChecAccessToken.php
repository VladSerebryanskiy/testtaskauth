<?php

namespace App\Http\Middleware;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Response;

class ChecAccessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return Response
     */
    public function handle($request, Closure $next) : Response
    {
        $token = $request->header('authorization');
        if (!$token)
            return response(null, 401);

        $user = User::where(['access_token' => $token])->first();

        if (!$user)
            return response(null, 404);

        if (Carbon::create($user->access_token_exp) < Carbon::now())
            return response(null, 401);

        return $next($request);
    }
}
