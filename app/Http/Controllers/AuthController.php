<?php

namespace App\Http\Controllers;

use App\Mail\authEmail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Illuminate\Support\Str;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * Time life of auth token
     */
    const AUTH_TOKEN_EXP_TIME = 240;

    /**
     * Time life of access token
     */
    const ACCESS_TOKEN_EXP_TIME = 240;

    /**
     * Time life of refresh token
     */
    const REFRESH_TOKEN_EXP_TIME = 43200;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(Request $request) : JsonResponse
    {
        $rules = [
            'email' => 'required|email',
        ];

        $body = $request->all();

        $validator = Validator::make($body, $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where(['email' => $request->email])->first();
        if (!$user) {
            return response()->json(null, 404);
        }

        $authToken = (string) Str::uuid();
        $user->auth_token = $authToken;
        $user->auth_token_exp = Carbon::now()->addMinutes(self::AUTH_TOKEN_EXP_TIME)->toDateTimeString();

        $user->save();

        $url = env('APP_URL') . '/api/login?auth_token=' . $authToken;

        try {
            $email = new authEmail($url);
            Mail::to($user->email)->send($email);
            return response()->json(null, 200);
        } catch (\Exception $e) {
            return response()->json(null, 400);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login() : JsonResponse
    {
        $authToken = request()->auth_token;

        $user = User::where([
            'auth_token' => $authToken,
        ])->first();

        if (!$user) {
            return response()->json(null, 404);
        }

        if (Carbon::create($user->auth_token_exp) < Carbon::now()) {
            return response()->json(null, 401);
        }

        $user->access_token = (string) Str::uuid();
        $user->access_token_exp = Carbon::now()->addMinutes(self::ACCESS_TOKEN_EXP_TIME)->toDateTimeString();
        $user->refresh_token = (string) Str::uuid();
        $user->refresh_token_exp = Carbon::now()->addDays(30)->toDateTimeString();

        $user->save();

        return response()->json([
            'access_token' => $user->access_token,
            'refresh_token' => $user->refresh_token,
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshTokens() : JsonResponse
    {
        $rules = [
            'refresh_token' => 'required|uuid',
        ];

        $body = request()->all();

        $validator = Validator::make($body, $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where([
            'refresh_token' => request()->refresh_token,
        ])->first();

        if (!$user) {
            return response()->json(null, 404);
        }

        if (Carbon::create($user->refresh_token_exp) < Carbon::now()) {
            return response()->json(null, 401);
        }

        $user->access_token = (string) Str::uuid();
        $user->access_token_exp = Carbon::now()->addMinutes(self::ACCESS_TOKEN_EXP_TIME)->toDateTimeString();
        $user->refresh_token = (string) Str::uuid();
        $user->refresh_token_exp = Carbon::now()->addMinutes(self::REFRESH_TOKEN_EXP_TIME)->toDateTimeString();

        $user->save();

        return response()->json([
            'access_token' => $user->access_token,
            'refresh_token' => $user->refresh_token,
        ], 200);

    }
}
